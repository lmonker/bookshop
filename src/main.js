import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import 'vant/lib/index.css'
import { Swipe, SwipeItem, Lazyload, Badge, Sidebar, SidebarItem,
    Collapse, CollapseItem, Tab, Tabs, Card, Form, Field, Image as VanImage,
    Button, SwipeCell, Cell, Stepper, RadioGroup, Radio, SubmitBar,
    Checkbox, Empty, Uploader, AddressList, AddressEdit, CheckboxGroup,ContactCard} from 'vant'

createApp(App).use(Swipe).use(SwipeItem).use(Badge)
    .use(Lazyload).use(Sidebar).use(SidebarItem).use(Collapse)
    .use(CollapseItem).use(Tab).use(Tabs).use(Card).use(Form)
    .use(Field).use(VanImage).use(Button).use(SwipeCell).use(Cell)
    .use(Stepper).use(RadioGroup).use(Radio).use(SubmitBar).use(Checkbox)
    .use(Empty).use(Uploader).use(AddressList).use(AddressEdit).use(CheckboxGroup)
    .use(ContactCard)
    .use(store).use(router).mount('#app');
