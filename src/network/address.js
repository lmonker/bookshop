import {request} from "./request";

//获取地址列表
export function getAddressList(data) {
    return request({
        url:'/api/address',
        method:'get',
        data
    })
}

//添加地址
export function addAddress(params) {
    return request({
        url: '/api/address',
        method: 'post',
        params
    })
}

//删除地址
export function deleteAddress(id) {
    return request({
        url: `/api/address/${id}`,
        method: 'delete',
    })
}