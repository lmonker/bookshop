import {request} from "./request";

//添加购物车
export function addCart(data) {
    return request({
        url:'/api/carts',
        method:'post',
        data,
    })
}

//修改购物车
export function changeCart(id, data) {
    return request({
        url:`/api/carts/${id}`,
        method:'put',
        data,
    })
}

//购物车改变选中
export function checkedCart(data) {
    return request({
        url:'/api/carts/checked',
        method:'patch',
        data,
    })
}

//获取购物车列表
export function getCartList(data) {
    return request({
        url: '/api/carts?' + data,
        method: 'get',
    })
}

//移出购物车
export function removeCart(id) {
    return request({
        url:`/api/carts/${id}`,
        method:'delete'
    })
}