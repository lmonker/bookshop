import {request} from "./request";

//获取分类所有数据
export function getCategoryAllData() {
    return request({
        url: '/api/goods',
        method:'get',
    })
}
export function getCategoryGoods(order="sales",page=1,cid=0) {
    return request({
        url: '/api/index?category_id='+cid+'&page='+page+'&'+order+'=1',
        method:'get',
    })
}