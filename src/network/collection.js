import {request} from "./request";

//获取收藏数据
export function getCollectionGoods(page,data) {
    return request({
        url: '/api/collects',
        method: 'get',
        data
    })
}

//添加收藏
export function collectionGoods(goods) {
    return request({
        url: `/api/collects/goods/${goods}`,
        method: 'post'
    })
}