import {request} from "./request";

//获取商品详情所有数据
export function getDetailData(id) {
    return request({
        url: '/api/goods/'+id,
        method:'get',
    })
}