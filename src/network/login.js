import {request} from "./request";

export function loginData(data) {
    return request({
        url: '/api/auth/login',
        method:'post',
        data//登录数据
    })
}

export function logout() {
    return request({
        url: '/api/auth/logout',
        method:'post',
    })
}