import {request} from "./request";

//订单预览
export function orderPreview() {
    return request({
        url: "/api/orders/preview",
        method: "get"
    })
}