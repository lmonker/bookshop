import axios from 'axios';
import { Toast } from 'vant';
// import {useRouter} from 'vue-router'
import router from 'vue-router';

export function request(config) {
    const instant = axios.create({
        baseURL: 'https://api.shop.eduwork.cn/',
        timeout: 5000
    });

    //请求拦截
    instant.interceptors.request.use(config => {
        const token = localStorage.getItem('token');
        // const router = useRouter();
        if(token){
            //添加请求头
            config.headers.Authorization = 'Bearer ' + token;
        }
        //直接放行
        return config;
    });
    //响应拦截
    instant.interceptors.response.use(res => {
        return res.data?res.data:res;
    }, err=>{
        if(err.response.status == 401){
            Toast('请先登录');
            router.push({path:'/login'});
        }
        // console.log(err.response.data);
        //错误提示  Object.keys(err.response.data.errors) 获得所有键
        Toast(err.response.data.errors[Object.keys(err.response.data.errors)[0]][0]);
    });
    return instant(config);
}