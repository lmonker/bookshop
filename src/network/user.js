import {request} from "./request";

export function register(data) {
    return request({
        url: '/api/auth/register',
        method: 'post',
        data//注册数据
    })
}

//获取用户信息
export function getUserInfo(data) {
    return request({
        url: '/api/user',
        method: 'get',
        data
    })
}

//更换头像
export function avatar() {
    return request({
        url: '/api/user/avatar',
        method: 'patch'
    })
}