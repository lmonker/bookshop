import { createRouter, createWebHistory} from 'vue-router'
const Home = () => import('../views/home/Home');
const Category = () => import('../views/category/category');
const ShopCart = () => import('../views/shopcart/shopcart');
const Profile = () => import('../views/profile/profile');
const Detail = () => import('../views/detail/detail');
const Register = () => import('../views/profile/Register');
const Login = () => import('../views/profile/login');
const Collection = () => import('../views/profile/Collection');
const Address = () => import('../views/profile/Address');
const AddressEdit = () => import('../views/profile/AddressEdit');
const OrderPreview = () => import('../views/order/OrderPreview');
import { Toast } from 'vant';

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    meta: {
      title:'首页'
    }
  },
  {
    path: '/category',
    name: 'Category',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: Category,
    meta: {
      title:'分类'
    }
  },
  {
    path: '/shopcart',
    name: 'ShopCart',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: ShopCart,
    meta: {
      title:'购物车',
      isAuthRequired:true
    }
  },
  {
    path: '/profile',
    name: 'Profile',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: Profile,
    meta: {
      title:'个人中心',
      isAuthRequired:true
    }
  },
  {
    path: '/detail',
    name: 'Detail',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: Detail,
    meta: {
      title:'商品详情'
    }
  },
  {
    path: '/register',
    name: 'Register',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: Register,
    meta: {
      title:'注册'
    }
  },
  {
    path: '/login',
    name: 'Login',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: Login,
    meta: {
      title:'登录'
    }
  },
  {
    path: '/collection',
    name: 'Collection',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: Collection,
    meta: {
      title:'收藏',
      isAuthRequired:true
    }
  },
  {
    path: '/address',
    name: 'Address',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: Address,
    meta: {
      title:'地址',
      isAuthRequired:true
    }
  },
  {
    path: '/addressEdit',
    name: 'AddressEdit',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: AddressEdit,
    meta: {
      title:'编辑地址',
      isAuthRequired:true
    }
  },
  {
    path: '/orderPreview',
    name: 'OrderPreview',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: OrderPreview,
    meta: {
      title:'订单预览',
      isAuthRequired:true
    }
  }
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
});

//路由前置守卫
router.beforeEach((to, from, next)=>{
  const token = localStorage.getItem('token');
  //指定页面登录后才可以访问
  if(to.meta.isAuthRequired && !token){
    Toast('请先登录');
    setTimeout(()=>{
      router.push({path:'/login'});
    },500);
  }else {
    next();
  }
});

//路由后置守卫
router.afterEach((to)=>{
  document.title = to.meta.title;
});
export default router
