import {getCartList} from "network/cart";

const actions = {

    updateCount({commit}){
        getCartList().then((res)=>{
            commit('addCart', {count:res.data.length});// 提交给 mutations
        })

    }
};

export default actions